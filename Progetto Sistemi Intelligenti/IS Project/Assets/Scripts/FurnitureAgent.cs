﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class FurnitureAgent : MonoBehaviour {

    //oggetto da piazzare (deve avere un mesh renderer per capire quali sono i suoi bordi)
    private FurnitureSO furniture;

    //vertici della zona da arredare
    List<Vector3> zoneVertices = new List<Vector3>();
    //oggetti tile della zona da arredare
    private List<GameObject> tiles = new List<GameObject>();
    //tile che collegano stanza a corridoi (da evitare per ora)
    private List<GameObject> joiningTiles = new List<GameObject>();

    //id di possibili parent che non vanno più bene
    private List<int> objectsID = new List<int>();

    //spazio dell'oggetto libero da altri oggetti per ogni faccia (si aggiornano i punti occupati)
    //0 = Vector3.up, 1 = Vector3.down, 2 = Vector3.forward, 3 = Vector3.back, 4 = Vector3.right, 5 = Vector3.left
    private Dictionary<Vector3, List<Vector3>> freeSurface = new Dictionary<Vector3, List<Vector3>>();

    //ASSUNZIONI: 
    //i modelli hanno sempre un MeshCollider, la forward è sempre rivolta nella direzione z positiva a livello di prefab,
    //i modelli ruotano solo su asse y, gli oggetti sui muri hanno sempre la forward rivolta verso l'interno della stanza


    //AGGIUNGERE PROGRESSBAR QUI O NEL PLACER (ULTIMA COSA)
    //OFFSET E LAYOUT SULLA FACCIA E PAVIMENTO/MURI/SOFFITTO
    public bool Initialize(FurnitureSO fso, List<Vector3> zone, List<GameObject> zoneTiles, List<GameObject> joiningTiles)
    {
        this.furniture = fso;
        this.zoneVertices = new List<Vector3>(zone);
        this.tiles = zoneTiles;
        this.joiningTiles = joiningTiles;

        //RemoveJoiningTiles();

        //posizionamento oggetto

        bool check = false;
        int attempts = 0;

        //finchè non lo riesco a posizionare correttamente o dopo un numero di tentativi
        while(!check && attempts < 20)
        {
            GameObject parent = GetPossibleParentInZone(zone);

            if (parent != null)
            {
                //se il parent è un tile
                if (tiles.Contains(parent))
                {
                    check = PlaceOnTile(parent);
                }
                else //se il parent è un mobile già piazzato
                {
                    check = PlaceOnFurniture(parent);
                }
            }

            attempts++;
        }

        if (!check)
        {
            DestroyImmediate(gameObject);
        }
        

        return true;
    }


    private bool PlaceOnTile(GameObject parent)
    {
        Vector3 pos = Vector3.zero;
        Vector3 yRot = Vector3.zero;

        if (parent.name.Equals("Floor"))
        {
            pos = GetRndPosBetweenCoor(zoneVertices);
        }
        else if (parent.name.Equals("Ceiling")) //qui si considera che forward sta in uno dei lati verticali
        {
            float height = 0;
            foreach(GameObject tile in tiles)
            {
                if (tile.name.Equals("Ceiling"))
                {
                    height = tile.transform.position.y;
                    break;
                }
            }
            List<Vector3> tmpList = new List<Vector3>();
            foreach(Vector3 v in zoneVertices)
            {
                tmpList.Add(new Vector3(v.x, height, v.z));
            }
            
            pos = GetRndPosBetweenCoor(tmpList);
            pos = new Vector3(pos.x, pos.y - ObjectHalfSize(Vector3.forward, "height"), pos.z);
        }
        else if (parent.name.Equals("Wall")) //oggetto va ruotato di conseguenza dato che viene piazzato nella sua rotazione da prefab
        {
            if (furniture.floor)
            {
                Bounds bounds = parent.GetComponent<MeshRenderer>().bounds;
                Vector3 min = bounds.min;
                Vector3 max = bounds.max;
                Vector3 otherSideMin;

                //per traslazione: (siccome tutti gli oggetti a muro devono essere rivolti dentro la stanza, si prende il lato sinistro o destro
                //per determinare di quanto viene  traslato)
                Vector3 traslation = Vector3.zero;

                //distinguo orientamento muro per rotazione e traslazione
                if (min.x == max.x)
                {
                    otherSideMin = new Vector3(min.x, min.y, max.z);

                    //controllo da che lato del muro si trova l'interno della stanza (stessa x)
                    foreach (GameObject tile in tiles)
                    {
                        if (tile.name.Equals("Floor"))
                        {
                            Bounds floorBounds = tile.GetComponent<MeshCollider>().bounds;
                            if (floorBounds.Intersects(bounds))
                            {
                                Vector3 closestPoint = floorBounds.ClosestPoint(parent.transform.position);
                                //se negativo su asse x allora va girato di -90 gradi su asse y
                                if (floorBounds.Contains(closestPoint + new Vector3(-1, 0, 0)))
                                {
                                    yRot = new Vector3(0, -90, 0);
                                    traslation = new Vector3(-ObjectHalfSize(Vector3.left, "depth"), 0, 0);
                                    break;
                                }
                                else //altrimenti va girato di 90 gradi su asse y
                                {
                                    yRot = new Vector3(0, 90, 0);
                                    traslation = new Vector3(ObjectHalfSize(Vector3.left, "depth"), 0, 0);
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    otherSideMin = new Vector3(max.x, min.y, min.z);

                    //controllo da che lato del muro si trova l'interno della stanza (stessa z)
                    foreach (GameObject tile in tiles)
                    {
                        if (tile.name.Equals("Floor"))
                        {
                            Bounds floorBounds = tile.GetComponent<MeshCollider>().bounds;
                            if (floorBounds.Intersects(bounds))
                            {
                                Vector3 closestPoint = floorBounds.ClosestPoint(parent.transform.position);
                                //se negativo su asse x allora va girato di -90 gradi su asse y
                                if (floorBounds.Contains(closestPoint + new Vector3(0, 0, -1)))
                                {
                                    yRot = new Vector3(0, 180, 0);
                                    traslation = new Vector3(0, 0, -ObjectHalfSize(Vector3.left, "depth"));
                                    break;
                                }
                                else //altrimenti va girato di 0 gradi su asse y
                                {
                                    yRot = new Vector3(0, 0, 0);
                                    traslation = new Vector3(0, 0, ObjectHalfSize(Vector3.left, "depth"));
                                    break;
                                }
                            }
                        }
                    }
                }

                List<Vector3> area = new List<Vector3>
                        {
                            min,
                            otherSideMin,
                        };

                pos = GetRndPosBetweenCoor(area);
                pos += traslation;
            }
            else if (furniture.ceiling) //rotazione e traslazione in base al muro + traslazione dal soffitto
            {
                Bounds bounds = parent.GetComponent<MeshRenderer>().bounds;
                Vector3 min = bounds.min;
                Vector3 max = bounds.max;
                Vector3 otherSideMax;

                //per traslazione: (siccome tutti gli oggetti a muro devono essere rivolti dentro la stanza, si prende il lato sinistro o destro
                //per determinare di quanto viene  traslato)
                Vector3 traslation = Vector3.zero;

                //distinguo orientamento muro per rotazione e traslazione
                if (min.x == max.x)
                {
                    otherSideMax = new Vector3(max.x, max.y, min.z);

                    //controllo da che lato del muro si trova l'interno della stanza (stessa x)
                    foreach (GameObject tile in tiles)
                    {
                        if (tile.name.Equals("Ceiling"))
                        {
                            Bounds ceilingBounds = tile.GetComponent<MeshCollider>().bounds;
                            if (ceilingBounds.Intersects(bounds))
                            {
                                Vector3 closestPoint = ceilingBounds.ClosestPoint(parent.transform.position);
                                //se negativo su asse x allora va girato di -90 gradi su asse y
                                if (ceilingBounds.Contains(closestPoint + new Vector3(-1, 0, 0)))
                                {
                                    yRot = new Vector3(0, -90, 0);
                                    traslation = new Vector3(-ObjectHalfSize(Vector3.left, "depth"), -ObjectHalfSize(Vector3.forward, "height"), 0);
                                    break;
                                }
                                else //altrimenti va girato di 90 gradi su asse y
                                {
                                    yRot = new Vector3(0, 90, 0);
                                    traslation = new Vector3(ObjectHalfSize(Vector3.left, "depth"), -ObjectHalfSize(Vector3.forward, "height"), 0);
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    otherSideMax = new Vector3(min.x, max.y, max.z);

                    //controllo da che lato del muro si trova l'interno della stanza (stessa z)
                    foreach (GameObject tile in tiles)
                    {
                        if (tile.name.Equals("Floor"))
                        {
                            Bounds floorBounds = tile.GetComponent<MeshCollider>().bounds;
                            if (floorBounds.Intersects(bounds))
                            {
                                Vector3 closestPoint = floorBounds.ClosestPoint(parent.transform.position);
                                //se negativo su asse x allora va girato di -90 gradi su asse y
                                if (floorBounds.Contains(closestPoint + new Vector3(0, 0, -1)))
                                {
                                    yRot = new Vector3(0, 180, 0);
                                    traslation = new Vector3(0, -ObjectHalfSize(Vector3.forward, "height"), -ObjectHalfSize(Vector3.left, "depth"));
                                    break;
                                }
                                else //altrimenti va girato di 0 gradi su asse y
                                {
                                    yRot = new Vector3(0, 0, 0);
                                    traslation = new Vector3(0, -ObjectHalfSize(Vector3.forward, "height"), ObjectHalfSize(Vector3.left, "depth"));
                                    break;
                                }
                            }
                        }
                    }
                }

                List<Vector3> area = new List<Vector3>
                        {
                            max,
                            otherSideMax,
                        };

                pos = GetRndPosBetweenCoor(area);
                pos += traslation;
            }
            else //caso solo muro, senza collegamenti a pavimento o soffitto
            {
                Bounds bounds = parent.GetComponent<MeshRenderer>().bounds;
                Vector3 min = bounds.min;
                Vector3 max = bounds.max;
                Vector3 bound1;
                Vector3 bound2;

                //per traslazione: (siccome tutti gli oggetti a muro devono essere rivolti dentro la stanza, si prende il lato sinistro o destro
                //per determinare di quanto viene  traslato)
                Vector3 traslation = Vector3.zero;

                //distinguo orientamento muro per rotazione e traslazione
                if (min.x == max.x)
                {
                    bound1 = new Vector3(min.x, min.y, max.z);
                    bound2 = new Vector3(min.x, max.y, min.z);

                    //controllo da che lato del muro si trova l'interno della stanza (stessa x)
                    foreach (GameObject tile in tiles)
                    {
                        if (tile.name.Equals("Floor"))
                        {
                            Bounds floorBounds = tile.GetComponent<MeshCollider>().bounds;
                            if (floorBounds.Intersects(bounds))
                            {
                                Vector3 closestPoint = floorBounds.ClosestPoint(parent.transform.position);
                                //se negativo su asse x allora va girato di -90 gradi su asse y
                                if (floorBounds.Contains(closestPoint + new Vector3(-1, 0, 0)))
                                {
                                    yRot = new Vector3(0, -90, 0);
                                    traslation = new Vector3(-ObjectHalfSize(Vector3.left, "depth"), 0, 0);
                                    break;
                                }
                                else //altrimenti va girato di 90 gradi su asse y
                                {
                                    yRot = new Vector3(0, 90, 0);
                                    traslation = new Vector3(ObjectHalfSize(Vector3.left, "depth"), 0, 0);
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    bound1 = new Vector3(max.x, min.y, min.z);
                    bound2 = new Vector3(min.x, max.y, min.z);

                    //controllo da che lato del muro si trova l'interno della stanza (stessa z)
                    foreach (GameObject tile in tiles)
                    {
                        if (tile.name.Equals("Floor"))
                        {
                            Bounds floorBounds = tile.GetComponent<MeshCollider>().bounds;
                            if (floorBounds.Intersects(bounds))
                            {
                                Vector3 closestPoint = floorBounds.ClosestPoint(parent.transform.position);
                                //se negativo su asse z allora va girato di 180 gradi su asse y
                                if (floorBounds.Contains(closestPoint + new Vector3(0, 0, -1)))
                                {
                                    yRot = new Vector3(0, 180, 0);
                                    traslation = new Vector3(0, 0, -ObjectHalfSize(Vector3.left, "depth"));
                                    break;
                                }
                                else //altrimenti va girato di 0 gradi su asse y
                                {
                                    yRot = new Vector3(0, 0, 0);
                                    traslation = new Vector3(0, 0, ObjectHalfSize(Vector3.left, "depth"));
                                    break;
                                }
                            }
                        }
                    }
                }

                List<Vector3> area = new List<Vector3>
                        {
                            min,
                            max,
                            bound1,
                            bound2
                        };

                pos = GetRndPosBetweenCoor(area);
                pos += traslation;
            }
        }

        GameObject furnitureObject = Instantiate(furniture.furnitureObject, pos, Quaternion.identity, gameObject.transform);
        furnitureObject.name = furniture.furnitureObject.name;
        furnitureObject.transform.Rotate(yRot);
        UpdateFacesLocation();

        //se oggetto collide con altro che non sia suo padre allora si elimina e si ricomincia la ricerca di una posizione adatta
        if (IsInRightPos(parent.GetInstanceID()))
        {
            return true;
        }
        else
        {
            //per ora si fa un figlio per faccia e basta
            DestroyImmediate(furnitureObject);
            objectsID.Add(parent.GetInstanceID());
        }

        return false;
    }

    private bool PlaceOnFurniture(GameObject parent)
    {
        FurnitureAgent parentAgent = parent.GetComponent<FurnitureAgent>();

        List<int> mySides = new List<int>();
        List<int> parentSides = new List<int>();

        for (int i = 0; i < furniture.parents.Length; i++)
        {
            if (furniture.parents[i].objectF.name.Equals(parent.transform.GetChild(0).name))
            {
                mySides.Add(furniture.parents[i].mySide);
                parentSides.Add(furniture.parents[i].parentSide);
            }
        }

        int maxAttempts = mySides.Count;

        for(int i = 0; i < maxAttempts; i++)
        {
            List<Vector3> parentSide = new List<Vector3>();

            switch (parentSides[i])
            {
                case 0:
                    parentSide = parentAgent.GetSide(Vector3.up);
                    break;
                case 1:
                    parentSide = parentAgent.GetSide(Vector3.down);
                    break;
                case 2:
                    parentSide = parentAgent.GetSide(Vector3.forward);
                    break;
                case 3:
                    parentSide = parentAgent.GetSide(Vector3.back);
                    break;
                case 4:
                    parentSide = parentAgent.GetSide(Vector3.right);
                    break;
                case 5:
                    parentSide = parentAgent.GetSide(Vector3.left);
                    break;
            }

            //calcolo centro lato
            Vector3 instaPos = new Vector3();
            //centro salvato per fare controllo rotazione, non viene traslato per la posizione
            Vector3 tmpCenter = new Vector3();

            //il posizionamento per tutti i lati prende solo il centro per ora, rotazione gestita solo per i lati
            //sopra
            if (parentSides[i] == 0)
            {
                instaPos = Vector3.Lerp(parentSide[0], parentSide[3], 0.5f);
            }
            else if (parentSides[i] == 1) //sotto
            {
                instaPos = Vector3.Lerp(parentSide[0], parentSide[3], 0.5f);
                instaPos = new Vector3(instaPos.x, instaPos.y - ObjectHalfSize(Vector3.forward, "height"), instaPos.z);
            }
            else //lati
            {
                //bisogna far si che la faccia di questo agente si allinei con quella dell'altro
                instaPos = Vector3.Lerp(parentSide[0], parentSide[3], 0.5f);
                instaPos = new Vector3(instaPos.x, Mathf.Min(parentSide[0].y, parentSide[3].y), instaPos.z);

                tmpCenter = instaPos;

                //calcolo metà larghezza (o lunghezza, da scegliere in base al lato) boundingBox per determinare quantità traslazione
                //prendo il lato perpendicolare a quello che dovrebbe combaciare con il parent per calcolare la distanza
                float halfSize = 0;

                switch (mySides[i])
                {
                    case 0:
                        Debug.Log("caso non gestito(u/d c)");
                        break;
                    case 1:
                        Debug.Log("caso non gestito(u/d c)");
                        break;
                    case 2:
                        halfSize = ObjectHalfSize(Vector3.left, "depth");
                        break;
                    case 3:
                        halfSize = ObjectHalfSize(Vector3.left, "depth");
                        break;
                    case 4:
                        halfSize = ObjectHalfSize(Vector3.forward, "height");
                        break;
                    case 5:
                        halfSize = ObjectHalfSize(Vector3.forward, "height");
                        break;
                }

                //per capire allineamento considerare cosa non cambia tra x e z, 
                //inoltre fare confronto con la coppia di lati del parent per ottenere il più o meno
                //(potrebbe non essere più valido)
                List<Vector3> otherParentSide = new List<Vector3>();

                switch (parentSides[i])
                {
                    case 0:
                        Debug.Log("caso non gestito(u/d p)");
                        break;
                    case 1:
                        Debug.Log("caso non gestito(u/d p)");
                        break;
                    case 2:
                        otherParentSide = parentAgent.GetSide(Vector3.back);
                        break;
                    case 3:
                        otherParentSide = parentAgent.GetSide(Vector3.forward);
                        break;
                    case 4:
                        otherParentSide = parentAgent.GetSide(Vector3.left);
                        break;
                    case 5:
                        otherParentSide = parentAgent.GetSide(Vector3.right);
                        break;
                }

                //se le x non cambiano allora devo traslare rispetto a x, se sono z allora rispetto a z
                if (parentSide[0].x == parentSide[3].x)
                {
                    //distinguo l'ordine della coppia di lati per capire orientamento dell'oggetto rispetto al mondo
                    if (parentSide[0].x > otherParentSide[0].x)
                    {
                        instaPos = new Vector3(instaPos.x + halfSize, instaPos.y, instaPos.z);
                    }
                    else
                    {
                        instaPos = new Vector3(instaPos.x - halfSize, instaPos.y, instaPos.z);
                    }
                }
                else if (parentSide[0].z == parentSide[3].z)
                {
                    if (parentSide[0].z > otherParentSide[0].z)
                    {
                        instaPos = new Vector3(instaPos.x, instaPos.y, instaPos.z + halfSize);
                    }
                    else
                    {
                        instaPos = new Vector3(instaPos.x, instaPos.y, instaPos.z - halfSize);
                    }
                }
            }

            GameObject furnitureObject = Instantiate(furniture.furnitureObject, instaPos, Quaternion.identity, gameObject.transform);
            furnitureObject.name = furniture.furnitureObject.name;

            //si usa questo per inizializzare la rotazione e poi si determina come girarlo sapendo che sta guardando il padre
            //si usa il centro di posizionamento del mio oggetto non ancora traslato
            Vector3 vec;

            furnitureObject.transform.LookAt(tmpCenter);
            vec = furnitureObject.transform.eulerAngles;
            vec.x = Mathf.Round(vec.x / 90) * 90;
            vec.y = Mathf.Round(vec.y / 90) * 90;
            vec.z = Mathf.Round(vec.z / 90) * 90;
            furnitureObject.transform.eulerAngles = vec;

            switch (mySides[i])
            {
                case 0:
                    //Debug.Log("ok");
                    break;
                case 1:
                    //Debug.Log("ok");
                    break;
                case 2:
                    break;
                case 3:
                    furnitureObject.transform.Rotate(0, 180, 0);
                    break;
                case 4:
                    furnitureObject.transform.Rotate(0, -90, 0);
                    break;
                case 5:
                    furnitureObject.transform.Rotate(0, 90, 0);
                    break;
            }

            UpdateFacesLocation();

            //se oggetto collide con altro che non sia suo padre allora si elimina e si ricomincia la ricerca di una posizione adatta
            if (IsInRightPos(parent.GetInstanceID()))
            {
                return true;
            }
            else
            {
                //per ora si fa un figlio per faccia e basta
                DestroyImmediate(furnitureObject);
                if(i == maxAttempts - 1)
                {
                    objectsID.Add(parent.GetInstanceID());
                }
            }
        }

        return false;
    }

    //si tiene in considerazione del sorting dei vertici per ogni lato e si passano sempre il forward per altezza e larghezza e il left per profondità
    private float ObjectHalfSize(Vector3 side, string depthHeightWidth)
    {
        //instanziazione temporanea per ricavare grandezza oggetto
        GameObject tmpObject = Instantiate(furniture.furnitureObject, new Vector3(0, 0, 0), Quaternion.identity, gameObject.transform);
        UpdateFacesLocation();

        List<Vector3> objectSide = new List<Vector3>(freeSurface[side]);
        DestroyImmediate(tmpObject);

        Vector3 firstPoint = Vector3.zero;
        Vector3 secondPoint = Vector3.zero;

        //depth
        if(depthHeightWidth.Equals("depth"))
        {
            firstPoint = objectSide[0];
            secondPoint = objectSide[1];
        }

        //height
        if (depthHeightWidth.Equals("height"))
        {
            firstPoint = objectSide[0];
            secondPoint = objectSide[1];
        }

        //width
        if (depthHeightWidth.Equals("width"))
        {
            firstPoint = objectSide[0];
            secondPoint = objectSide[2];
        }

        return Vector3.Distance(firstPoint, secondPoint) / 2;
    }

    //ottengo agente di un possibile parente nella zona
    private GameObject GetPossibleParentInZone(List<Vector3> vertices)
    {
        GameObject[] agents = GameObject.FindGameObjectsWithTag("FurnitureAgent");
        foreach (GameObject agent in agents)
        {
            if (!agent.Equals(gameObject) && !objectsID.Contains(agent.GetInstanceID()))
            {
                Transform furnitureP = agent.transform.GetChild(0);
                if (IsPointInArea(furnitureP.position, vertices))
                {
                    foreach (ParentSideConnection obj in this.furniture.parents)
                    {
                        if (furnitureP.name.Equals(obj.objectF.name))
                        {
                            return agent;
                        }
                    }
                }
            }
        }


        //se non trovo padri tra gli oggetti già piazzati controllo per pavimento, soffitto e muri
        if (furniture.floor)
        {
            //se ho solo il pavimento come padre ritorno un tile random del pavimento
            if (!furniture.wall)
            {
                return GetTileParent("Floor");
            }
            else //se ho anche il muro ritorno un tile random del muro
            {
                return GetTileParent("Wall");
            }
        }

        if (furniture.ceiling)
        {
            //se ho solo il soffitto come padre ritorno un tile random del soffitto
            if (!furniture.wall)
            {
                return GetTileParent("Ceiling");
            }
            else //se ho anche il muro ritorno un tile random del muro
            {
                return GetTileParent("Wall");
            }
        }

        //se ho solo muro come padre
        if(furniture.wall && !furniture.ceiling && !furniture.floor)
        {
            return GetTileParent("Wall");
        }

        return null;
    }

    private GameObject GetTileParent(string tileType)
    {
        List<GameObject> tmpTiles = new List<GameObject>();
        foreach (GameObject tile in tiles)
        {
            if (tile.name.Equals(tileType) && !objectsID.Contains(tile.GetInstanceID()))
            {
                tmpTiles.Add(tile);
            }
        }

        if (tmpTiles.Count != 0)
        {
            return tmpTiles[Random.Range(0, tmpTiles.Count)];
        }

        return null;
    }

    //controllo che non ci siano altri oggetti nello spazio in cui si va a posizionare l'oggetto (a parte il padre a cui starà appicciato)
    //controllo di tutti i tile della stanza più tutti gli altri oggetti della stanza
    private bool IsInRightPos(int parentInstanceID)
    {
        MeshCollider myFurnitureCollider = gameObject.transform.GetChild(0).GetComponent<MeshCollider>();
        List<string> myParentsNames = new List<string>();
        for (int i = 0; i < furniture.parents.Length; i++)
        {
            myParentsNames.Add(furniture.parents[i].objectF.name);
        }

        //controllo che non si appoggi su tile non permessi
        foreach (GameObject tile in tiles)
        {
            if (myFurnitureCollider.bounds.Intersects(tile.GetComponent<MeshCollider>().bounds))
            {
                if (tile.name.Equals("Floor"))
                {
                    if (!furniture.floor)
                    {
                        return false;
                    }

                    if (joiningTiles.Contains(tile))
                    {
                        return false;
                    }
                }
                if (tile.name.Equals("Wall") && !furniture.wall)
                {
                    return false;
                }
                if (tile.name.Equals("Ceiling") && !furniture.ceiling)
                {
                    return false;
                }
            }
        }

        //controllo con altri agenti che non siano il possibile parent
        GameObject[] agents = GameObject.FindGameObjectsWithTag("FurnitureAgent");
        foreach (GameObject agent in agents)
        {
            if (!agent.Equals(gameObject))
            {
                MeshCollider otherAgentCollider = agent.transform.GetChild(0).GetComponent<MeshCollider>();
                if (myFurnitureCollider.bounds.Intersects(otherAgentCollider.bounds))
                {
                    if (parentInstanceID != agent.GetInstanceID())
                    {
                        return false;
                    }
                }
            }
        }

        List<Vector3> frontSideBounds = freeSurface[Vector3.forward];
        List<Vector3> backSideBounds = freeSurface[Vector3.back];

        foreach(GameObject tile in joiningTiles) //i joiningTiles sono solo tile del pavimento
        {
            Vector3 min = tile.GetComponent<MeshCollider>().bounds.min;
            Vector3 max = tile.GetComponent<MeshCollider>().bounds.max;
            Vector3 bound1 = new Vector3(min.x, min.y, max.z);
            Vector3 bound2 = new Vector3(max.x, min.y, min.z);

            List<Vector3> vertices = new List<Vector3> {
                min,
                max,
                bound1,
                bound2
            };

            foreach (Vector3 boundPoint in frontSideBounds)
            {
                if (IsPointInArea(boundPoint, vertices))
                {
                    return false;
                }
            }

            foreach (Vector3 boundPoint in backSideBounds)
            {
                if (IsPointInArea(boundPoint, vertices))
                {
                    return false;
                }
            }

        }

        bool check = true;

        foreach(Vector3 boundPoint in frontSideBounds)
        {
            if(!IsPointInArea(boundPoint, zoneVertices))
            {
                check = false;
            }
        }

        foreach (Vector3 boundPoint in backSideBounds)
        {
            if (!IsPointInArea(boundPoint, zoneVertices))
            {
                check = false;
            }
        }

        return check;
    }

    //se x o z non cambiano si considera altezza e larghezza (dato che il forward esce sempre dal muro per ora)
    //se y non cambia si considera larghezza e profondità
    //invece di usare le grandezze dell'oggetto verificare se tutti i suoi bounds sono all'interno dell'intera stanza o corridoio
    private Vector3 GetRndPosBetweenCoor(List<Vector3> vertices)
    {
        Vector3 pos = Vector3.zero;

        //0=x,1=y,2=z
        int nonChangingAxis;
        float marginX = 0;
        float marginY = 0;
        float marginZ = 0;

        Vector3 xCenter = Vector3.zero;
        Vector3 yCenter = Vector3.zero;
        Vector3 zCenter = Vector3.zero;

        if (vertices.Count == 4) //può essere muro, pavimento, soffitto o uno dei lati di un prefab già piazzato
        {
            if(vertices[0].x == vertices[1].x && vertices[1].x == vertices[2].x && vertices[2].x == vertices[3].x)
            {
                nonChangingAxis = 0;
            }
            else if (vertices[0].y == vertices[1].y && vertices[1].y == vertices[2].y && vertices[2].y == vertices[3].y)
            {
                nonChangingAxis = 1;
            }
            else
            {
                nonChangingAxis = 2;
            }
        }
        else //può essere solo un bordo che collega muro con pavimento o soffitto e forse con uno dei lati di un prefab già piazzato
        {
            if (vertices[0].x == vertices[1].x)
            {
                nonChangingAxis = 0;
            }
            else if (vertices[0].y == vertices[1].y)
            {
                nonChangingAxis = 1;
            }
            else
            {
                nonChangingAxis = 2;
            }
        }

        //calcolo margine per oggetto
        if (nonChangingAxis == 0)
        {
            marginY = ObjectHalfSize(Vector3.forward, "height");
            marginZ = ObjectHalfSize(Vector3.forward, "width");
        }
        else if (nonChangingAxis == 1)
        {
            marginX = ObjectHalfSize(Vector3.forward, "width");
            marginZ = ObjectHalfSize(Vector3.left, "depth");
        }
        else
        {
            marginX = ObjectHalfSize(Vector3.forward, "width");
            marginY = ObjectHalfSize(Vector3.forward, "height");
        }

        //se gli mando solo 2 punti necessito di una posizione random sulla retta che li collega
        if (vertices.Count == 2)
        {
            if(vertices[0].x == vertices[1].x)
            {
                if(vertices[0].y == vertices[1].y)
                {
                    if (furniture.centeredZorXZ)
                    {
                        Vector3 center = Vector3.Lerp(vertices[0], vertices[1], 0.5f);
                        pos = center;
                    }
                    else
                    {
                        pos = new Vector3(vertices[0].x, vertices[0].y, Random.Range(vertices[0].z + marginZ, vertices[1].z - marginZ));
                    }
                }
                else
                {
                    if (furniture.centeredXorY)
                    {
                        Vector3 center = Vector3.Lerp(vertices[0], vertices[1], 0.5f);
                        pos = center;
                    }
                    else
                    {
                        pos = new Vector3(vertices[0].x, Random.Range(vertices[0].y + marginY, vertices[1].y - marginY), vertices[0].z);
                    }
                }
            }
            if(vertices[0].y == vertices[1].y)
            {
                if(vertices[0].x == vertices[1].x)
                {
                    if (furniture.centeredZorXZ)
                    {
                        Vector3 center = Vector3.Lerp(vertices[0], vertices[1], 0.5f);
                        pos = center;
                    }
                    else
                    {
                        pos = new Vector3(vertices[0].x, vertices[0].y, Random.Range(vertices[0].z + marginZ, vertices[1].z - marginZ));
                    }
                }
                else
                {
                    if (furniture.centeredZorXZ)
                    {
                        Vector3 center = Vector3.Lerp(vertices[0], vertices[1], 0.5f);
                        pos = center;
                    }
                    else
                    {
                        pos = new Vector3(Random.Range(vertices[0].x + marginX, vertices[1].x - marginX), vertices[0].y, vertices[0].z);
                    }
                }
            }
            if(vertices[0].z == vertices[1].z)
            {
                if(vertices[0].y == vertices[1].y)
                {
                    if (furniture.centeredZorXZ)
                    {
                        Vector3 center = Vector3.Lerp(vertices[0], vertices[1], 0.5f);
                        pos = center;
                    }
                    else
                    {
                        pos = new Vector3(Random.Range(vertices[0].x + marginX, vertices[1].x - marginX), vertices[0].y, vertices[0].z);
                    }
                }
                else
                {
                    if (furniture.centeredXorY)
                    {
                        Vector3 center = Vector3.Lerp(vertices[0], vertices[1], 0.5f);
                        pos = center;
                    }
                    else
                    {
                        pos = new Vector3(vertices[0].x, Random.Range(vertices[0].y + marginY, vertices[1].y - marginY), vertices[0].z);
                    }
                }
            }
        }
        else //altrimenti sull'area
        {
            if (Mathf.Round(vertices[0].y) == Mathf.Round(vertices[1].y)) //arrotondamento per il pavimento
            {
                for(int i = 0; i < vertices.Count; i++)
                {
                    if (vertices[0].x != vertices[i].x && vertices[0].z != vertices[i].z)
                    {
                        List<Vector3> tmpList = new List<Vector3> { vertices[0], vertices[i] };
                        tmpList.Sort(CoordSort);
                        Vector3 center = Vector3.Lerp(tmpList[0], tmpList[1], 0.5f);
                        if (furniture.centeredXorY && furniture.centeredZorXZ)
                        {
                            pos = center;
                        }
                        else if (furniture.centeredXorY && !furniture.centeredZorXZ)
                        {
                            pos = new Vector3(center.x, tmpList[0].y, Random.Range(tmpList[0].z + marginZ, tmpList[1].z - marginZ));
                        }
                        else if (!furniture.centeredXorY && furniture.centeredZorXZ)
                        {
                            pos = new Vector3(Random.Range(tmpList[0].x + marginX, tmpList[1].x - marginX), tmpList[0].y, center.z);
                        }
                        else
                        {
                            pos = new Vector3(Random.Range(tmpList[0].x + marginX, tmpList[1].x - marginX), tmpList[0].y, Random.Range(tmpList[0].z + marginZ, tmpList[1].z - marginZ));
                        }
                        break;
                    }
                }
            }
            else
            {
                if (vertices[0].x == vertices[1].x)
                {
                    for (int i = 0; i < vertices.Count; i++)
                    {
                        if (vertices[0].y != vertices[i].y && vertices[0].z != vertices[i].z)
                        {
                            List<Vector3> tmpList = new List<Vector3> { vertices[0], vertices[i] };
                            tmpList.Sort(CoordSort);
                            Vector3 center = Vector3.Lerp(tmpList[0], tmpList[1], 0.5f);
                            if (furniture.centeredXorY && furniture.centeredZorXZ)
                            {
                                pos = center;
                            }
                            else if (furniture.centeredXorY && !furniture.centeredZorXZ)
                            {
                                pos = new Vector3(tmpList[0].x, center.y, Random.Range(tmpList[0].z + marginZ, tmpList[1].z - marginZ));
                            }
                            else if (!furniture.centeredXorY && furniture.centeredZorXZ)
                            {
                                pos = new Vector3(tmpList[0].x, Random.Range(tmpList[0].y + marginY, tmpList[1].y - marginY), center.z);
                            }
                            else
                            {
                                pos = new Vector3(tmpList[0].x, Random.Range(vertices[0].y + marginY, tmpList[1].y - marginY), Random.Range(tmpList[0].z + marginZ, tmpList[1].z - marginZ));
                            }
                            break;
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < vertices.Count; i++)
                    {
                        if (vertices[0].y != vertices[i].y && vertices[0].x != vertices[i].x)
                        {
                            List<Vector3> tmpList = new List<Vector3> { vertices[0], vertices[i] };
                            tmpList.Sort(CoordSort);
                            Vector3 center = Vector3.Lerp(tmpList[0], tmpList[1], 0.5f);
                            if (furniture.centeredXorY && furniture.centeredZorXZ)
                            {
                                pos = center;
                            }
                            else if (furniture.centeredXorY && !furniture.centeredZorXZ)
                            {
                                pos = new Vector3(Random.Range(tmpList[0].x + marginX, tmpList[1].x - marginX), center.y, tmpList[0].z);
                            }
                            else if (!furniture.centeredXorY && furniture.centeredZorXZ)
                            {
                                pos = new Vector3(center.x, Random.Range(tmpList[0].y + marginY, tmpList[1].y - marginY), tmpList[0].z);
                            }
                            else
                            {
                                pos = new Vector3(Random.Range(tmpList[0].x + marginX, tmpList[1].x - marginX), Random.Range(tmpList[0].y + marginY, tmpList[1].y - marginY), tmpList[0].z);
                            }
                            break;
                        }
                    }
                }
            }
        }

        return pos;
    }

    public FurnitureSO GetFurnitureSO()
    {
        return this.furniture;
    }


    //controllo se la posizione di un punto è in una determinata area
    private bool IsPointInArea(Vector3 pos, List<Vector3> vertices)
    {
        foreach (Vector3 vertexX in vertices)
        {
            if (vertices[0].x != vertexX.x)
            {
                foreach (Vector3 vertexZ in vertices)
                {
                    if (vertices[0].z != vertexZ.z)
                    {
                        if (vertices[0].x < vertexX.x)
                        {
                            if(vertices[0].z < vertexZ.z)
                            {
                                if(vertices[0].x < pos.x && vertexX.x >= pos.x)
                                {
                                    if (vertices[0].z < pos.z && vertexZ.z >= pos.z)
                                    {
                                        return true;
                                    }
                                }
                            }
                            else
                            {
                                if (vertices[0].x < pos.x && vertexX.x >= pos.x)
                                {
                                    if (vertices[0].z >= pos.z && vertexZ.z < pos.z)
                                    {
                                        return true;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (vertices[0].z < vertexZ.z)
                            {
                                if (vertices[0].x >= pos.x && vertexX.x < pos.x)
                                {
                                    if (vertices[0].z < pos.z && vertexZ.z >= pos.z)
                                    {
                                        return true;
                                    }
                                }
                            }
                            else
                            {
                                if (vertices[0].x >= pos.x && vertexX.x < pos.x)
                                {
                                    if (vertices[0].z >= pos.z && vertexZ.z < pos.z)
                                    {
                                        return true;
                                    }
                                }
                            }
                        }

                        break;
                    }
                }

                break;
            }
        }

        return false;
    }

    //aggiorna la posizione dei bounds e identifica i lati della bounding box
    private void UpdateFacesLocation()
    {
        freeSurface.Clear();

        MeshCollider borders = transform.GetChild(0).GetComponent<MeshCollider>();

        //i vertici del MeshCollider (con locazione a rotazione 0)
        Vector3 boundPoint1 = borders.bounds.min; //angolo in basso a sinistra del retro
        Vector3 boundPoint2 = borders.bounds.max; //angolo in alto a destra del fronte
        Vector3 boundPoint3 = new Vector3(boundPoint1.x, boundPoint1.y, boundPoint2.z); //angolo in basso a sinistra del fronte
        Vector3 boundPoint4 = new Vector3(boundPoint1.x, boundPoint2.y, boundPoint1.z); //angolo in alto a sinistra del retro
        Vector3 boundPoint5 = new Vector3(boundPoint2.x, boundPoint1.y, boundPoint1.z); //angolo in basso a destra del retro
        Vector3 boundPoint6 = new Vector3(boundPoint1.x, boundPoint2.y, boundPoint2.z); //angolo in alto a sinistra del fronte
        Vector3 boundPoint7 = new Vector3(boundPoint2.x, boundPoint1.y, boundPoint2.z); //angolo in basso a destra del fronte
        Vector3 boundPoint8 = new Vector3(boundPoint2.x, boundPoint2.y, boundPoint1.z); //angolo in alto a destra del retro

        //solo per rotazione su asse y, per ora
        Vector3 normalY = new Vector3(0, 1, 0);

        //organizzo i vertici per identificare i lati

        List<Vector3> sidePoints = new List<Vector3>();

        //sopra
        sidePoints.Add(boundPoint6);
        sidePoints.Add(boundPoint2);
        sidePoints.Add(boundPoint8);
        sidePoints.Add(boundPoint4);

        sidePoints.Sort(CoordSort);

        freeSurface.Add(Vector3.up, new List<Vector3>(sidePoints));

        sidePoints.Clear();

        //sotto
        sidePoints.Add(boundPoint3);
        sidePoints.Add(boundPoint7);
        sidePoints.Add(boundPoint5);
        sidePoints.Add(boundPoint1);

        sidePoints.Sort(CoordSort);

        freeSurface.Add(Vector3.down, new List<Vector3>(sidePoints));

        sidePoints.Clear();

        //avanti
        sidePoints.Add(boundPoint6);
        sidePoints.Add(boundPoint3);
        sidePoints.Add(boundPoint2);
        sidePoints.Add(boundPoint7);

        Vector3 side1 = boundPoint3 - boundPoint6;
        Vector3 side2 = boundPoint2 - boundPoint6;
        Vector3 normal = Vector3.Cross(side1, side2).normalized;
        int angle = Mathf.RoundToInt(AngleSigned(transform.GetChild(0).transform.forward, normal, normalY));

        sidePoints.Sort(CoordSort);

        switch (angle)
        {
            case 0:
                freeSurface.Add(Vector3.forward, new List<Vector3>(sidePoints));
                break;
            case 90:
                freeSurface.Add(Vector3.right, new List<Vector3>(sidePoints));
                break;
            case -90:
                freeSurface.Add(Vector3.left, new List<Vector3>(sidePoints));
                break;
            case 180:
                freeSurface.Add(Vector3.back, new List<Vector3>(sidePoints));
                break;
            case -180:
                freeSurface.Add(Vector3.back, new List<Vector3>(sidePoints));
                break;
        }

        sidePoints.Clear();

        //dietro
        sidePoints.Add(boundPoint8);
        sidePoints.Add(boundPoint5);
        sidePoints.Add(boundPoint4);
        sidePoints.Add(boundPoint1);

        side1 = boundPoint5 - boundPoint8;
        side2 = boundPoint4 - boundPoint8;
        normal = Vector3.Cross(side1, side2).normalized;
        angle = Mathf.RoundToInt(AngleSigned(transform.GetChild(0).transform.forward, normal, normalY));

        sidePoints.Sort(CoordSort);

        switch (angle)
        {
            case 0:
                freeSurface.Add(Vector3.forward, new List<Vector3>(sidePoints));
                break;
            case 90:
                freeSurface.Add(Vector3.right, new List<Vector3>(sidePoints));
                break;
            case -90:
                freeSurface.Add(Vector3.left, new List<Vector3>(sidePoints));
                break;
            case 180:
                freeSurface.Add(Vector3.back, new List<Vector3>(sidePoints));
                break;
            case -180:
                freeSurface.Add(Vector3.back, new List<Vector3>(sidePoints));
                break;
        }

        sidePoints.Clear();

        //destra
        sidePoints.Add(boundPoint2);
        sidePoints.Add(boundPoint7);
        sidePoints.Add(boundPoint8);
        sidePoints.Add(boundPoint5);

        side1 = boundPoint7 - boundPoint2;
        side2 = boundPoint8 - boundPoint2;
        normal = Vector3.Cross(side1, side2).normalized;
        angle = Mathf.RoundToInt(AngleSigned(transform.GetChild(0).transform.forward, normal, normalY));

        sidePoints.Sort(CoordSort);

        switch (angle)
        {
            case 0:
                freeSurface.Add(Vector3.forward, new List<Vector3>(sidePoints));
                break;
            case 90:
                freeSurface.Add(Vector3.right, new List<Vector3>(sidePoints));
                break;
            case -90:
                freeSurface.Add(Vector3.left, new List<Vector3>(sidePoints));
                break;
            case 180:
                freeSurface.Add(Vector3.back, new List<Vector3>(sidePoints));
                break;
            case -180:
                freeSurface.Add(Vector3.back, new List<Vector3>(sidePoints));
                break;
        }

        sidePoints.Clear();

        //sinistra
        sidePoints.Add(boundPoint6);
        sidePoints.Add(boundPoint3);
        sidePoints.Add(boundPoint4);
        sidePoints.Add(boundPoint1);

        side1 = boundPoint4 - boundPoint6; //invertito volontariamente dopo diversi test, attenzione a possibili comportamenti anomali in alcuni casi
        side2 = boundPoint1 - boundPoint6;
        normal = Vector3.Cross(side1, side2).normalized;
        angle = Mathf.RoundToInt(AngleSigned(transform.GetChild(0).transform.forward, normal, normalY));

        sidePoints.Sort(CoordSort);

        switch (angle)
        {
            case 0:
                freeSurface.Add(Vector3.forward, new List<Vector3>(sidePoints));
                break;
            case 90:
                freeSurface.Add(Vector3.right, new List<Vector3>(sidePoints));
                break;
            case -90:
                freeSurface.Add(Vector3.left, new List<Vector3>(sidePoints));
                break;
            case 180:
                freeSurface.Add(Vector3.back, new List<Vector3>(sidePoints));
                break;
            case -180:
                freeSurface.Add(Vector3.back, new List<Vector3>(sidePoints));
                break;
        }

        sidePoints.Clear();
    }

    private float AngleSigned(Vector3 v1, Vector3 v2, Vector3 normal)
    {
        return Mathf.Atan2(Vector3.Dot(normal, Vector3.Cross(v1, v2)), Vector3.Dot(v1, v2)) * Mathf.Rad2Deg;
    }

    //funzione sorting dando priorità a x, poi y e infine z, mettendo i valori uguali vicini per asse
    private int CoordSort(Vector3 a, Vector3 b)
    {
        int xdiff = a.x.CompareTo(b.x);
        if (xdiff != 0)
        {
            return xdiff;
        }
        else
        {
            int ydiff = a.y.CompareTo(b.y);
            if (ydiff != 0)
            {
                return ydiff;
            }
            else
            {
                int zdiff = a.z.CompareTo(b.z);
                return zdiff;
            }
        }
    }

    public List<Vector3> GetSide(Vector3 requestedSide)
    {
        return freeSurface[requestedSide];
    }
}
