﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

public class FurniturePlacer : MonoBehaviour {

    public GameObject furnitureAgent;
    public List<FurnitureSO> totalFurniture;

    //i tile che compongono sia le stanze che i corridoi
    private Dictionary<GameObject, List<GameObject>> rooms = new Dictionary<GameObject, List<GameObject>>();
    private Dictionary<GameObject, List<GameObject>> hallways = new Dictionary<GameObject, List<GameObject>>();

    //i vertici di stanze e corridoi
    private Dictionary<Transform, List<Vector3>> roomsVertices;
    private Dictionary<Transform, List<Vector3>> hallwaysVertices;

    private List<GameObject> joiningTiles = new List<GameObject>();

    private List<GameObject> agents = new List<GameObject>();

    public void Initialize(Dictionary<Transform, List<Vector3>> roomsV, Dictionary<Transform, List<Vector3>> hallwaysV, Dictionary<GameObject, List<GameObject>> roomsT, Dictionary<GameObject, List<GameObject>> hallwaysT, List<GameObject> joiningTiles)
    {
        this.rooms = roomsT;
        this.hallways = hallwaysT;
        this.roomsVertices = roomsV;
        this.hallwaysVertices = hallwaysV;
        this.joiningTiles = joiningTiles;

        Dictionary<string, List<FurnitureSO>> furnitureGroupRooms = new Dictionary<string, List<FurnitureSO>>();
        Dictionary<string, List<FurnitureSO>> furnitureGroupHallways = new Dictionary<string, List<FurnitureSO>>();

        foreach (FurnitureSO furniture in totalFurniture)
        {
            if (!furniture.IsHallway)
            {
                if (!furnitureGroupRooms.ContainsKey(furniture.roomType))
                {
                    furnitureGroupRooms.Add(furniture.roomType, new List<FurnitureSO>());
                    for (int i = 0; i < furniture.quantityPerRoom; i++)
                    {
                        furnitureGroupRooms[furniture.roomType].Add(furniture);
                    }
                }
                else
                {
                    for (int i = 0; i < furniture.quantityPerRoom; i++)
                    {
                        furnitureGroupRooms[furniture.roomType].Add(furniture);
                    }
                }
            }
            else
            {
                if (!furnitureGroupHallways.ContainsKey(furniture.roomType))
                {
                    furnitureGroupHallways.Add(furniture.roomType, new List<FurnitureSO>());
                    for (int i = 0; i < furniture.quantityPerRoom; i++)
                    {
                        furnitureGroupHallways[furniture.roomType].Add(furniture);
                    }
                }
                else
                {
                    for (int i = 0; i < furniture.quantityPerRoom; i++)
                    {
                        furnitureGroupHallways[furniture.roomType].Add(furniture);
                    }
                }
            }
        }

        foreach (GameObject room in rooms.Keys)
        {
            List<GameObject> roomTiles = rooms[room];
            int roomSize = 0;
            foreach (GameObject tile in roomTiles)
            {
                if (tile.name.Equals("Floor"))
                {
                    roomSize++;
                }
            }

            List<string> keys = new List<string>(furnitureGroupRooms.Keys);
            string randomRoomType = keys[Random.Range(0, keys.Count)];
            ShuffleFurnitureRnd(furnitureGroupRooms[randomRoomType]);
            OrganizeFurniture(furnitureGroupRooms[randomRoomType]);

            int furnitureNumber = furnitureGroupRooms[randomRoomType].Count;
            int multiplier = Mathf.CeilToInt(roomSize / furnitureNumber);
            if(multiplier == 0)
            {
                multiplier = 1;
            }

            foreach (FurnitureSO f in furnitureGroupRooms[randomRoomType])
            {
                for(int i = 0; i < multiplier; i++)
                {
                    StartCoroutine(Agent(f, roomsVertices[room.transform], roomTiles));
                }
            }
        }

        foreach (GameObject hallway in hallways.Keys)
        {
            List<GameObject> hallwayTiles = hallways[hallway];
            int hallwaySize = 0;
            foreach (GameObject tile in hallwayTiles)
            {
                if (tile.name.Equals("Floor"))
                {
                    hallwaySize++;
                }
            }

            List<string> keys = new List<string>(furnitureGroupHallways.Keys);
            string randomHallwayType = keys[Random.Range(0, keys.Count)];
            ShuffleFurnitureRnd(furnitureGroupHallways[randomHallwayType]);
            OrganizeFurniture(furnitureGroupHallways[randomHallwayType]);

            int furnitureNumber = furnitureGroupHallways[randomHallwayType].Count;
            int multiplier = Mathf.CeilToInt(hallwaySize / furnitureNumber);
            if (multiplier == 0)
            {
                multiplier = 1;
            }

            foreach (FurnitureSO f in furnitureGroupHallways[randomHallwayType])
            {
                for (int i = 0; i < multiplier; i++)
                {
                    StartCoroutine(Agent(f, hallwaysVertices[hallway.transform], hallwayTiles));
                }
            }
        }
    }

    private IEnumerator Agent(FurnitureSO furniture, List<Vector3> zoneVertices, List<GameObject> zoneTiles)
    {
        GameObject agent = Instantiate(furnitureAgent, new Vector3(0, 0, 0), Quaternion.identity, transform);

        GameObject instantiatedRooms = GameObject.Find("Rooms");

        List<GameObject> roomJT = new List<GameObject>();
        foreach (GameObject t in joiningTiles)
        {
            if (zoneTiles.Contains(t))
            {
                roomJT.Add(t);
            }
        }

        yield return new WaitUntil(() => agent.GetComponent<FurnitureAgent>().Initialize(furniture, zoneVertices, zoneTiles, roomJT));
    }

    private void ShuffleFurnitureRnd(List<FurnitureSO> list)
    {
        int count = list.Count;
        int last = count - 1;
        for (int i = 0; i < last; i++)
        {
            int r = Random.Range(i, count);
            FurnitureSO tmp = list[i];
            list[i] = list[r];
            list[r] = tmp;
        }
    }

    private void OrganizeFurniture(List<FurnitureSO> list)
    {
        list.Sort((x, y) => x.parents.Length.CompareTo(y.parents.Length));
    }
}
