﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu(fileName = "Furniture", menuName = "Furniture")]
public class FurnitureSO : ScriptableObject {

    public GameObject furnitureObject;
    public bool IsHallway;
    public string roomType;
    public int quantityPerRoom;
    public bool floor;
    public bool ceiling;
    public bool wall;

    public bool centeredXorY;
    public bool centeredZorXZ;

    //0 = sopra, 1 = sotto, 2 = fronte, 3 = dietro, 4 = destra, 5 = sinistra
    //va aggiunto il parent per ogni coppia di facce a cui si può connettere
    public ParentSideConnection[] parents;

    private void OnValidate()
    {
        if (floor)
        {
            ceiling = false;
        }
        if (ceiling)
        {
            floor = false;
        }
    }
}

[System.Serializable]
public struct ParentSideConnection
{
    //0 = sopra, 1 = sotto, 2 = fronte, 3 = dietro, 4 = destra, 5 = sinistra
    public int mySide;
    public int parentSide;
    //public int offset;
    public GameObject objectF;
}
