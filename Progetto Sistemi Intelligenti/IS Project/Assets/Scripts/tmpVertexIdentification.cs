﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tmpVertexIdentification : MonoBehaviour {

    public GameObject o;

	// Use this for initialization
	void Start () {
        Instantiate(o);
	}
	
	// Update is called once per frame
	void Update () {

        MeshRenderer collider = o.GetComponent<MeshRenderer>();

        //i vertici del boxcollider (con locazioni a rotazione 0)
        Vector3 boundPoint1 = collider.bounds.min; //angolo in basso a sinistra del retro
        Vector3 boundPoint2 = collider.bounds.max; //angolo in alto a destra del fronte
        Vector3 boundPoint3 = new Vector3(boundPoint1.x, boundPoint1.y, boundPoint2.z); //angolo in basso a sinistra del fronte
        Vector3 boundPoint4 = new Vector3(boundPoint1.x, boundPoint2.y, boundPoint1.z); //angolo in alto a sinistra del retro
        Vector3 boundPoint5 = new Vector3(boundPoint2.x, boundPoint1.y, boundPoint1.z); //angolo in basso a destra del retro
        Vector3 boundPoint6 = new Vector3(boundPoint1.x, boundPoint2.y, boundPoint2.z); //angolo in alto a sinistra del fronte
        Vector3 boundPoint7 = new Vector3(boundPoint2.x, boundPoint1.y, boundPoint2.z); //angolo in basso a destra del fronte
        Vector3 boundPoint8 = new Vector3(boundPoint2.x, boundPoint2.y, boundPoint1.z); //angolo in alto a destra del retro

        Color lineColor = Color.green;
        // rectangular cuboid
        // top of cube (6-2-8-4)
        Debug.DrawLine(boundPoint6, boundPoint2, lineColor);
        lineColor = Color.yellow;
        Debug.DrawLine(boundPoint2, boundPoint8, lineColor);
        lineColor = Color.red;
        Debug.DrawLine(boundPoint8, boundPoint4, lineColor);
        lineColor = Color.blue;
        Debug.DrawLine(boundPoint4, boundPoint6, lineColor);

        lineColor = Color.green;

        // bottom of cube (3-7-5-1)
        Debug.DrawLine(boundPoint3, boundPoint7, lineColor);
        lineColor = Color.yellow;
        Debug.DrawLine(boundPoint7, boundPoint5, lineColor);
        lineColor = Color.red;
        Debug.DrawLine(boundPoint5, boundPoint1, lineColor);
        lineColor = Color.blue;
        Debug.DrawLine(boundPoint1, boundPoint3, lineColor);

        lineColor = Color.blue;

        // legs (6-3, 2-7, 8-5, 4-1)
        //front
        Debug.DrawLine(boundPoint6, boundPoint3, lineColor);
        Debug.DrawLine(boundPoint2, boundPoint7, lineColor);

        lineColor = Color.yellow;

        //back
        Debug.DrawLine(boundPoint8, boundPoint5, lineColor);
        Debug.DrawLine(boundPoint4, boundPoint1, lineColor);

        lineColor = Color.blue;

        //right
        Debug.DrawLine(boundPoint2, boundPoint7, lineColor);
        Debug.DrawLine(boundPoint8, boundPoint5, lineColor);

        lineColor = Color.yellow;

        //left
        Debug.DrawLine(boundPoint6, boundPoint3, lineColor);
        Debug.DrawLine(boundPoint4, boundPoint1, lineColor);
    }
}
